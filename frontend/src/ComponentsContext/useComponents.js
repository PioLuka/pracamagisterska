import {useContext, useEffect} from 'react';
import ComponentsContext from '.';
import Component from '../Classes/Component';
import { MUIComponents } from '../Components/MUIComponents';
import MUIIconsRegister from '../Components/MUIComponents/MUIIconsRegister';
import { setPath } from '../Utils/objectPath';

const useComponents = () => {
    const {
        components,
        setComponents,
        draggedComponent,
        setDraggedComponent, 
        selectedComponent, 
        setSelectedComponent,
        reactCode,
        isPreview,
        setIsPreview,
        componentsTree,

        cssCode,
        isCssEnabled,
        setIsCssEnabled,
    } = useContext(ComponentsContext);

    const handleAddComponent = (component) => {
        setComponents(prev => [...prev, component]);
        setSelectedComponent(component);
    };

    const handleRemoveComponent = (id) => {
        const parentComponent = components.find(c => c.id === components.find(c => c.id === id).parentId);
        let idsToRemove = [];
        let childrenIds = components.filter(c => c.parentId === id).map(c => c.id);

        childrenIds.forEach(compId => {
            idsToRemove = idsToRemove.concat(childrenCheck(compId));
        });

        idsToRemove.push(id);

        setComponents(components.filter(c => !idsToRemove.includes(c.id)));
        setSelectedComponent(parentComponent);
    };

    const handleUpdateComponent = (id, editedComponent) => {
        //const tmpComponents = [...components];
        setComponents(components.map(i => i.id !== id ? i : editedComponent));
    };

    const childrenCheck = (id) => {
        let idsToRemove = [];
        let childrenIds = components.filter(c => c.parentId === id).map(c => c.id);
        
        childrenIds.forEach(compId => {
            idsToRemove = idsToRemove.concat(childrenCheck(compId));
        });
        idsToRemove.push(id);
        return idsToRemove;
    };

    const handleSetSelectedComponent = (event, id) => {
        event.preventDefault();
        event.stopPropagation();

        if(isPreview){
            return;
        }

        if(selectedComponent?.id !== id){
            setSelectedComponent(components.find(c => c.id === id));
        }
    };

    const handleStylesChange = (id, name, value) => {
        const tmpComponents = [...components];
        const tmpComponent = tmpComponents.find(c => c.id === id);
        //tmpComponent.styles[name] = value;
        setPath(tmpComponent.styles, name, value);

        setComponents(components.map(i => i.id !== id ? i : tmpComponent));
    };

    const handlePropsChange = (id, name, value) => {
        const tmpComponents = [...components];
        const tmpComponent = tmpComponents.find(c => c.id === id);
        tmpComponent.props[name] = value;

        setComponents(components.map(i => i.id !== id ? i : tmpComponent));
    };
    
    const handleDrop = (event, parentId, parentComponent) => {
        event.stopPropagation();

        // console.log('parentId => ' + parentId)
        // console.log('dr id => ' + draggedComponent.id)
        // console.log('dr parentId => ' + draggedComponent.parentId)

        if(!draggedComponent.id){
            const newComponent = new Component(
                draggedComponent.component, 
                draggedComponent.name,
                draggedComponent.allowChildren,
                draggedComponent.styles,
                draggedComponent.props,
                draggedComponent.imports,
                parentId,
                draggedComponent.deleteWithParent
            );

            if(parentComponent){
                handleAddComponent(parentComponent);
            }
            handleAddComponent(newComponent);
        } else {
            const pComponent = components.find(c => c.id === parentId);
            if(parentId !== draggedComponent.id && draggedComponent.parentId !== parentId && pComponent?.parentId !== draggedComponent.id){
                let newComponents = [...components];
                const component = newComponents.find(c => c.id === draggedComponent.id);
                component.parentId = parentId;
                newComponents = newComponents.filter(x => x.id !== component.id);

                if(parentComponent){
                    newComponents.push(parentComponent);
                }
                newComponents.push(component);
    
                setComponents(newComponents);
            }
        }
    };
    
    const allowDrop = (event) => {
        event.preventDefault();
    };

    const onDragStart = (event, component) => {
        event.stopPropagation();

        if(isPreview){
            return;
        }

        setDraggedComponent(component);
    }

    const loadDemo = (demoJSON) => {
        const tmpComponents = JSON.parse(demoJSON);
        const demoComponents = [];
        tmpComponents.forEach(c => {
            let comp = MUIComponents.find(x => x.name === c.name);

            if(!comp){
                comp = MUIIconsRegister.find(x => x.name === c.name);
            }

            if(comp){
                const newComponent = new Component(
                    comp.component, 
                    c.name,
                    c.allowChildren,
                    c.styles,
                    c.props,
                    c.imports,
                    c.parentId,
                    c.deleteWithParent,
                    c.id
                );
    
                demoComponents.push(newComponent);
            }
        });
        setComponents(demoComponents);
    };

    const handleSetIsPreview = (value) => {
        setIsPreview(value);
    };

    const makeComponent = (comp, withoutProps) => {
        return (
            comp &&
            !withoutProps ?
                <comp.component
                    props = {comp.props}
                    styles = {comp.styles}
                    id = {comp.id}

                    onClick={(event) => handleSetSelectedComponent(event, comp.id)}
                    classes={`${!isPreview ? (selectedComponent?.id === comp.id ? 'selected-item-border' : 'not-selected-item-border') : ''}`}
                    onDragStart={(event) => onDragStart(event, comp)}
                    draggable={!isPreview}
                /> :
                <comp.component />
        );
    };

    return {
        components: components,
        handleAdd: handleAddComponent,
        handleRemove: handleRemoveComponent,
        handleUpdate: handleUpdateComponent,
        draggedComponent: draggedComponent,
        setDraggedComponent: (component) => setDraggedComponent(component),
        handleSetSelectedComponent: handleSetSelectedComponent,
        selectedComponent: selectedComponent,
        reactCode: reactCode,

        cssCode,
        isCssEnabled,
        setIsCssEnabled,

        changeStyles: handleStylesChange,
        changeProps: handlePropsChange,
        handleDrop: handleDrop,
        allowDrop: allowDrop,
        onDragStart,
        makeComponent: makeComponent,
        loadDemo: loadDemo,

        setIsPreview: handleSetIsPreview,
        componentsTree: componentsTree
    };
};

export default useComponents;