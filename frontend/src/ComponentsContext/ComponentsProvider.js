import React, { useEffect, useState } from 'react';
import ComponentsContext from '.';

const reactCodeTemplate = `
import React from 'react';
#imports#

const Template = () => {
    return (
        <>#body#</>
    );
};

export default Template;
`;

const importsTemplates = {
    mui: `import {#imports#} from '@mui/material';`,
    muiIcons: `import {#imports#} from '@mui/icons-material';`
};

const stylesTemplate = `sx={{#styles#}}`;

const ComponentsProvider = (props) => {
    const {
        children
    } = props;

    const [components, setComponents] = useState([]);
    const [draggedComponent, setDraggedComponent] = useState(null);
    const [selectedComponent, setSelectedComponent] = useState(undefined);
    const [reactCode, setReactCode] = useState('');
    const [cssCode, setCssCode] = useState('');
    const [isPreview, setIsPreview] = useState(false);
    const [isCssEnabled, setIsCssEnabled] = useState(false);

    const [componentsTree, setComponentsTree] = useState([]);

    useEffect(() => {
        generateReactCode();
        generateComponentsTree();
        generateCssCode();
    }, [components, isCssEnabled]);

    const generateReactCode = () => {
        let generatedCode = '';
        let reactCodeTemplateCopy = reactCodeTemplate;

        generatedCode += generateComponentCode(components.filter(c => c.parentId === undefined));

        reactCodeTemplateCopy = reactCodeTemplateCopy.replace('#body#', generatedCode.trim());
        reactCodeTemplateCopy = reactCodeTemplateCopy.replace('#imports#', generateImoprtsCode().trim());

        setReactCode(reactCodeTemplateCopy);
    };

    const generateComponentPropsCode = (props) => {
        let propsString = '';
        for (const [key, value] of Object.entries(props)) {
            if (key[0] !== '_' && key !== 'text') {
                propsString += value !== '' ? ` ${key.split('_')[0]}='${value}'` : '';
            }
        }
        return propsString;
    };

    const generateImoprtsCode = () => {
        let imports = {};

        components.forEach(c => {
            if (c.imports) {
                for (const [key, value] of Object.entries(c.imports)) {
                    value.forEach(x => {
                        if (!imports.hasOwnProperty(key)) {
                            imports[key] = new Set();
                            imports[key].add(x);
                        } else {
                            imports[key].add(x);
                        }
                    })
                }
            }
        });

        let importsString = '';

        for (const [key, value] of Object.entries(imports)) {
            let valuesString = '';
            value.forEach(v => valuesString += `${v},`);
            importsString += `${importsTemplates[key].replace('#imports#', valuesString)} \n`;
        }

        if(isCssEnabled) {
            importsString += `import './Template.css'; \n`;
        }

        return importsString;
    };

    const generateComponentCode = (comps) => {
        let generatedCode = '';

        comps?.forEach(c => {
            generatedCode += `<${c.name}${generateComponentPropsCode(c.props)} ${!isCssEnabled ? generateComponentStylesCode(c.styles) : `className='${c.name}_${c.id.replaceAll('-', '')}'`}`;

            if(c.allowChildren || c.props.text){
                generatedCode += `>${!c.props.text ? generateComponentCode(components.filter(x => x.parentId === c.id)) : c.props.text}</${c.name}>`;
            } else {
                generatedCode += '/>';
            }
        });

        return generatedCode;
    };

    const generateComponentStylesCode = (styles) => {
        const templateCopy = stylesTemplate;
        let stylesString = '';
        for (const [key, value] of Object.entries(styles)) {
            if (typeof value !== 'object') {
                stylesString += value !== '' ? `${key}:'${value}',` : '';
            } else {
                const innerStyleTemplate = `'&:${key}':{#innerStyle#},`
                const innerStyle = `${Object.entries(value).map(
                    v => { return v[1] !== '' ? v[0] + ':' + `'${v[1]}'` : '' }
                )}`;
                stylesString += innerStyle !== '' ? innerStyleTemplate.replace('#innerStyle#', innerStyle) : '';
            }

        }
        return stylesString !== '' ? templateCopy.replace('#styles#', stylesString) : '';
    };

    const generateComponentsTree = () => {
        setComponentsTree(generateComponentsTreeNode(components.filter(c => c.parentId === undefined)));
    };

    const generateComponentsTreeNode = (comps) => {
        const nodes = [];
        comps?.forEach(c => {
            nodes.push({
                name: c.name,
                id: c.id,
                children: generateComponentsTreeNode(components.filter(x => x.parentId === c.id))
            });
        });

        return nodes;
    };

    const generateCssCode = () => {
        let generatedCode = '';

        components.filter(c => Object.keys(c.styles).length > 0).forEach(c => {
            let classCode = '';

            Object.entries(c.styles).forEach(([key, value]) => {
                if (typeof value !== 'object') {
                    classCode += value !== '' ? `${makeCssClassName(key)}:'${value}';` : '';
                } else {
                    const innerStyleTemplate = `'&:${makeCssClassName(key)}'{#innerStyle#};`
                    const innerStyle = `${Object.entries(value).map(
                        v => { return v[1] !== '' ? makeCssClassName(v[0]) + ':' + `'${v[1]}'` : '' }
                    )}`;
                    classCode += innerStyle !== '' ? innerStyleTemplate.replace('#innerStyle#', innerStyle) : '';
                }
            });

            generatedCode += `.${c.name}_${c.id.replaceAll('-', '')} {${classCode}}\n\n`;
        });

        setCssCode(generatedCode.replaceAll('\'', ''));
    };

    const makeCssClassName = (reactClassName) => {
        return reactClassName.replace(/[A-Z][a-z]*/g, str => '-' + str.toLowerCase() + '-')
            .replace('--', '-')
            .replace(/(^-)|(-$)/g, '');
    };

    return (
        <ComponentsContext.Provider
            value={{
                components,
                setComponents,
                draggedComponent,
                setDraggedComponent,
                selectedComponent,
                setSelectedComponent,
                reactCode,

                cssCode,
                isCssEnabled,
                setIsCssEnabled,
                
                isPreview,
                setIsPreview,

                componentsTree
            }}
        >
            {children}
        </ComponentsContext.Provider>
    );
};

export default ComponentsProvider;

