import ColorInput from "../../Components/PortalComponents/ColorInput";
import SettingsHover from "../../Components/PortalComponents/SettingsHover";
import SettingsSelect from "../../Components/PortalComponents/SettingsSelect";
import SizeInput from "../../Components/PortalComponents/SizeInput";
import TextInput from "../../Components/PortalComponents/TextInput";

const StylesRegister = [
    {
        name: 'color',
        component: ColorInput
    },
    {
        name: 'backgroundColor',
        component: ColorInput
    },
    {
        name: 'width',
        component: TextInput
    },
    {
        name: 'height',
        component: TextInput
    },
    // {
    //     name: 'width',
    //     component: SizeInput
    // },
    // {
    //     name: 'height',
    //     component: SizeInput
    // },
    {
        name: 'hover',
        component: SettingsHover
    },
    {
        name: 'textAlign',
        component: SettingsSelect,
        values: ['center', 'left', 'right', 'justify']
    },
    // {
    //     name: 'marginTop',
    //     component: SizeInput
    // },
    // {
    //     name: 'marginRight',
    //     component: SizeInput
    // },
    // {
    //     name: 'marginBottom',
    //     component: SizeInput
    // },
    // {
    //     name: 'marginLeft',
    //     component: SizeInput
    // },
    // {
    //     name: 'paddingTop',
    //     component: SizeInput
    // },
    // {
    //     name: 'paddingRight',
    //     component: SizeInput
    // },
    // {
    //     name: 'paddingBottom',
    //     component: SizeInput
    // },
    // {
    //     name: 'paddingLeft',
    //     component: SizeInput
    // },
    {
        name: 'margin',
        component: TextInput
    },
    {
        name: 'padding',
        component: TextInput
    },
    {
        name: 'display',
        component: SettingsSelect,
        values: ['block', 'inline', 'flex']
    },
    {
        name: 'flexDirection',
        component: SettingsSelect,
        values: ['row', 'row-reverse', 'column', 'column-reverse']
    },
    {
        name: 'alignItems',
        component: SettingsSelect,
        values: ['start', 'center', 'end', 'flex-start', 'flex-end']
    },
    {
        name: 'justifyContent',
        component: SettingsSelect,
        values: ['start', 'center', 'end', 'flex-start', 'flex-end', 'left', 'right']
    },
    {
        name: 'gap',
        component: TextInput
    },
    {
        name: 'fontWeight',
        component: TextInput
    },
    {
        name: 'fontSize',
        component: TextInput
    }
];

export default StylesRegister;