import MUIIconsRegister from '../../Components/MUIComponents/MUIIconsRegister';
import BooleanSwitch from '../../Components/PortalComponents/BooleanSwitch';
import NumberInput from '../../Components/PortalComponents/NumberInput';
import SettingsSelect from '../../Components/PortalComponents/SettingsSelect'
import TextInput from '../../Components/PortalComponents/TextInput';

const PropsRegister = [
    {
        name: 'variant_1',
        component: SettingsSelect,
        values: ['text', 'outlined', 'contained']
    },
    {
        name: 'size',
        component: SettingsSelect,
        values: ['small', 'medium', 'large']
    },
    {
        name: 'direction',
        component: SettingsSelect,
        values: ['column-reverse', 'column', 'row-reverse', 'row']
    },
    {
        name: 'justifyContent',
        component: SettingsSelect,
        values: ['flex-start', 'center', 'flex-end', 'space-between', 'space-around', 'space-evenly']
    },
    {
        name: 'alignItems',
        component: SettingsSelect,
        values: ['flex-start', 'center', 'flex-end', 'stretch', 'baseline']
    },
    {
        name: 'spacing',
        component: NumberInput
    },
    {
        name: 'text',
        component: TextInput
    },
    {
        name: 'itemsCount',
        component: NumberInput
    },
    {
        name: 'variant_2',
        component: SettingsSelect,
        values: ['standard', 'outlined', 'filled']
    },
    {
        name: '_showFormHelperText',
        component: BooleanSwitch,
    },
    {
        name: 'fullWidth',
        component: BooleanSwitch,
    },
    {
        name: 'maxWidth',
        component: SettingsSelect,
        values: ['xs', 'sm', 'md', 'lg', 'xl']
    },
    {
        name: 'type',
        component: SettingsSelect,
        values: ['text', 'password', 'tel', 'email']
    },
    {
        name: '_iconName',
        component: SettingsSelect,
        values: MUIIconsRegister.map(x => x.name)
    },
    {
        name: 'underline',
        component: SettingsSelect,
        values: ['none', 'hover', 'always']
    }
];

export default PropsRegister;