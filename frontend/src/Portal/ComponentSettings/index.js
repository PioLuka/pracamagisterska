import { Button, Link } from "@mui/material";
import React, { useEffect, useState } from "react";
import useComponents from "../../ComponentsContext/useComponents";
import './index.css';
import PropsRegister from "./PropsRegister";
import StylesRegister from "./StylesRegister";

const ComponentSettings = (props) => {

    const {
        selectedComponent,
        handleRemove,
        handleSetSelectedComponent,
        components
    } = useComponents();

    const [parentName, setParentName] = useState(undefined);

    useEffect(() => {
        setParentName(components.filter(c => c.id === selectedComponent?.parentId)[0]?.name);
    }, [selectedComponent]);

    const makeStylesSubTab = (name, selectedComponent) => {
        const Style = StylesRegister.find(r => r.name === name);

        if (Style) {
            const Component = Style.component;
            return (
                <Component 
                    name={name} 
                    component={selectedComponent} 
                    values={Style.values}
                    isStyle={true}
                />
            );
        } else {
            return name;
        }
    };


    const makePropsSubTab = (name, selectedComponent) => {
        const Property = PropsRegister.find(r => r.name === name);

        if (Property) {
            const Component = Property.component;
            return (
                <Component 
                    name={name} 
                    component={selectedComponent} 
                    values={Property.values} 
                    isProperty={true}
                />
            );
        } else {
            return name;
        }
    };

    const deleteSelectedComponent = () => {
        handleRemove(selectedComponent.id);
    };

    const switchToComponent = (event, componentId) => {
        if(!componentId){
            return;
        }

        handleSetSelectedComponent(event, componentId);
    };

    return (
        <React.Fragment>
            <div className='components-menu-header'>Component settings</div>
            {
                selectedComponent &&
                <React.Fragment>
                    {
                        Object.keys(selectedComponent.styles).length > 0 &&
                        <React.Fragment>
                            <div className='componentsSettingsTab'>
                                Styles
                            </div>
                            {
                                Object.keys(selectedComponent.styles).map((key, index) => {
                                    return (
                                        <div className='componentsSettingsSubTab' key={index}>
                                            {makeStylesSubTab(key, selectedComponent)}
                                        </div>
                                    );
                                })
                            }
                        </React.Fragment>
                    }
                    {
                        Object.keys(selectedComponent.props).length > 0 &&
                        <React.Fragment>
                            <div className='componentsSettingsTab'>
                                Props
                            </div>
                            {
                                Object.keys(selectedComponent.props).map((key, index) => {
                                    return (
                                        <div className='componentsSettingsSubTab' key={index}>
                                            {makePropsSubTab(key, selectedComponent)}
                                        </div>
                                    );
                                })
                            }
                        </React.Fragment>
                    }
                    <React.Fragment>
                        <div className='componentsSettingsTab'>
                            Parent
                        </div>
                        <Link
                            onClick={(event) => switchToComponent(event, selectedComponent.parentId)}
                            className={`${parentName ? 'componentsSettingsLink' : 'componentsSettingsLink-parentNone'}`}
                        >
                            {parentName || 'None'}
                        </Link>
                    </React.Fragment>
                    <React.Fragment>
                        <div className='componentsSettingsTab'>
                            Children
                        </div>
                        {
                            components.filter(c => c.parentId === selectedComponent.id).map((comp, index) => {
                                return (
                                    <Link
                                        key={index}
                                        onClick={(event) => switchToComponent(event, comp.id)}
                                        className={`componentsSettingsLink`}
                                    >
                                        {comp.name}
                                    </Link>
                                );
                            })
                        }
                    </React.Fragment>
                    <div className="componentsSettingsButton">
                        {
                            <Button
                                children="Delete Selected Component"
                                variant="contained"
                                sx={{
                                    //color: 'var(--text-color)',
                                    color: '#F2F1EB',
                                    backgroundColor: '#800000',
                                    '&:hover': {
                                        backgroundColor: '#800000',
                                        opacity: '0.75'
                                    },
                                    fontWeight: '600'
                                }}
                                onClick={deleteSelectedComponent}
                                disabled={selectedComponent.deleteWithParent}
                            />
                        }
                    </div>
                </React.Fragment>
            }
        </React.Fragment>
    );
};

export default ComponentSettings;