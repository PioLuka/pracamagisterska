import { AppBar, Button, Dialog, IconButton, Toolbar, Tooltip, Typography } from "@mui/material";
import React, { useState } from "react";
import Slide from '@mui/material/Slide';
import CloseIcon from '@mui/icons-material/Close';
import ComponentsDesigner from "../ComponentsDesigner";
import useComponents from "../../ComponentsContext/useComponents";
import { Devices, MobileFriendly } from "@mui/icons-material";

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const PortalHeader = () => {
    const {
        setIsPreview
    } = useComponents();

    const [open, setOpen] = useState(false);
    const [isMobileView, setIsMobileView] = React.useState(false);

    const openPreview = () => {
        setOpen(true);
        setIsPreview(true);
    };

    const closePreview = () => {
        setOpen(false);
        setIsPreview(false);
    };

    const handleChangeViewType = () => {
        setIsMobileView(!isMobileView);
    };

    return (
        <React.Fragment>
            <Button
                sx={{
                    margin: '0.5rem',
                    color: '#cccccc',
                    backgroundColor: '#408386',
                    borderColor: '#67cdcc',
                    '&:hover': {
                        borderColor: '#408386',
                        backgroundColor: '#408386',
                        opacity: '70%'
                    },
                    fontSize: '0.6rem',
                    fontWeight: 600,
                    minWidth: '4.5rem'
                }}
                //onClick={saveCodeToClipboard}
                variant='outlined'
                size="small"
                onClick={openPreview}
            >
                Open Preview
            </Button>

            <Dialog
                fullScreen
                open={open}
                onClose={closePreview}
                TransitionComponent={Transition}
            >
                <AppBar sx={{ position: 'relative', backgroundColor: '#f5f2f0', color: 'var(--text-color)'}}>
                    <Toolbar style={{display: "flex"}}>
                        <div style={{display: "flex", alignItems: 'center' }}>
                            <Typography sx={{ mr: '0.5rem' }} variant="h6" component="div">
                                Preview
                            </Typography>
                            <IconButton
                                style={{marginLeft: 'auto'}}
                                edge="end"
                                onClick={handleChangeViewType}
                                aria-label="close"
                                sx={{color: isMobileView ? '#408386' : 'var(--text-color)'}}
                            >
                                <Tooltip title={isMobileView ? 'Close mobile view' : 'Open mobile view'}>
                                    <MobileFriendly />
                                </Tooltip>
                            </IconButton>
                        </div>
                        <IconButton
                            style={{marginLeft: 'auto'}}
                            edge="end"
                            color="inherit"
                            onClick={closePreview}
                            aria-label="close"
                        >
                            <CloseIcon />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', height: '100%' }}>
                    <div style={{ height: isMobileView ? '812px' : '100%', width: isMobileView ? '375px' : '100%', maxWidth: isMobileView ? '375px' : '100%' }}>
                        <ComponentsDesigner isPreview={true} />
                    </div>
                </div>
            </Dialog>
        </React.Fragment>
    );
};

export default PortalHeader;