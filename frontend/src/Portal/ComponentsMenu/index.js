import React from 'react';
import Divider from '../../Components/PortalComponents/Divider';
import { MUIComponents } from '../../Components/MUIComponents';

import './index.css';
import useComponents from '../../ComponentsContext/useComponents';
import MUIComponentsList from './MUIComponentsList';

const ComponentsMenu = () => {
    const {
        onDragStart,
        components
    } = useComponents();

    return (
        <React.Fragment>
            <div className='components-menu-header'>MUI Components</div>
            {/* <Divider /> */}
            {/* {
                MUIComponents.filter(x => x.code !== false).map((component, index) => (
                    <React.Fragment
                        key={index}
                    >
                        <div 
                            className='components-menu-tab' 
                            draggable={true}
                            onDragStart={(event) => onDragStart(event, component)}
                        >
                            {component.name}
                        </div>
                        <Divider />
                    </React.Fragment>
                ))
            } */}
            <MUIComponentsList />
            {/* <Divider />
            <div className='components-menu-header'>Demos</div> */}
        </React.Fragment>
    );
};

export default ComponentsMenu;
