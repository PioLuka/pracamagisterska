import { Collapse, IconButton, List, ListItem, ListItemText, ListSubheader } from "@mui/material";
import React, { useState } from "react";
import useComponents from "../../ComponentsContext/useComponents";
import { MUIComponentsRegister } from './MUIComponentsRegister';
import LinkIcon from '@mui/icons-material/Link';
import { MUIComponents } from "../../Components/MUIComponents";

const MUIComponentsList = () => {
    const { onDragStart } = useComponents();

    return (
        MUIComponentsRegister.map((group, index) => (
            <List
                sx={{ 
                    width: '100%',
                    paddingBottom: 0
                }}
                subheader={
                    <ListSubheader 
                        component="div" 
                        sx={{
                            backgroundColor: 'var(--base-color)', 
                            color: 'var(--text-color)',
                            fontWeight: '600',
                            lineHeight: '36px'
                        }}
                    >
                        {group.groupName}
                    </ListSubheader>
                }
                dense={true}
                key={index}
            >
                {
                    group.components.map((component, index) => (
                        <ListItem
                            key={index}
                            secondaryAction={
                                <IconButton edge='start' href={component.docsUrl} target='_blank'>
                                    <LinkIcon />
                                </IconButton>
                            }
                            sx={{ 
                                cursor: 'move', 
                                '&:hover' : {
                                    //backgroundColor: '#333333',
                                    backgroundColor: '#e5e5e5'
                                }
                            }}
                            draggable={true}
                            onDragStart={(event) =>
                                onDragStart(event, MUIComponents.find(x => x.name === component.name))
                            }
                        >
                            <ListItemText
                                primary={component.name}
                            />
                        </ListItem>
                    ))
                }
            </List>
        ))
    );
};

export default MUIComponentsList;