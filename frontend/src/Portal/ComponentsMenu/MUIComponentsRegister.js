export const MUIComponentsRegister = [
    {
        groupName: 'Inputs',
        components: [
            {
                name: 'Button',
                docsUrl: 'https://mui.com/material-ui/react-button/'
            },
            {
                name: 'FormControl',
                docsUrl: 'https://mui.com/material-ui/api/form-control/'
            },
            {
                name: 'IconButton',
                docsUrl: 'https://mui.com/material-ui/api/icon-button/'
            }
        ]
    },
    {
        groupName: 'Data Display',
        components: [
            {
                name: 'Typography',
                docsUrl: 'https://mui.com/material-ui/react-typography/'
            }
        ]
    },
    {
        groupName: 'Layout',
        components: [
            {
                name: 'Box',
                docsUrl: 'https://mui.com/material-ui/react-box/'
            },
            {
                name: 'Container',
                docsUrl: 'https://mui.com/material-ui/react-container/'
            },
            // {
            //     name: 'Grid',
            //     docsUrl: 'https://mui.com/material-ui/react-grid/'
            // }
        ]
    },
    {
        groupName: 'Navigation',
        components: [
            {
                name: 'Link',
                docsUrl: 'https://mui.com/material-ui/react-link/'
            }
        ]
    }
];
