import React, { useEffect } from 'react';
import useComponents from '../../ComponentsContext/useComponents';

const demoJSON = '[{"id":"b524a55f-5fcb-42a3-965a-ab2ea18b8a64","name":"Box","allowChildren":true,"styles":{"width":"100%","height":"100%","backgroundColor":"#E0E0E0","margin":"","padding":"","display":"flex","flexDirection":"column","gap":"","alignItems":"center","justifyContent":"center"},"props":{},"imports":{"mui":["Box"]}},{"id":"cd7f0d1e-808d-4886-a38e-0bde9388a8d9","name":"Container","allowChildren":true,"styles":{"margin":"","padding":""},"props":{"maxWidth":"xs"},"imports":{"mui":["Container"]},"parentId":"b524a55f-5fcb-42a3-965a-ab2ea18b8a64"},{"id":"2b277970-52bc-4f9b-84a5-1c6140907ebd","name":"Box","allowChildren":true,"styles":{"width":"auto","height":"auto","backgroundColor":"#C9CCD8","margin":"","padding":"3rem 2rem","display":"block","flexDirection":"column","gap":"","alignItems":"","justifyContent":""},"props":{},"imports":{"mui":["Box"]},"parentId":"cd7f0d1e-808d-4886-a38e-0bde9388a8d9"},{"id":"af6cd04c-005b-49ce-adf0-110b65c97615","name":"Typography","allowChildren":false,"styles":{"color":"#1976D2","textAlign":"center","margin":"0 0 2rem","padding":"","fontWeight":"600","fontSize":"2rem"},"props":{"text":"Login"},"imports":{"mui":["Typography"]},"parentId":"2b277970-52bc-4f9b-84a5-1c6140907ebd"},{"id":"891e0162-bc7b-47e8-afd1-f2ea90e8075c","name":"FormControl","allowChildren":true,"styles":{"margin":"0 0 0.5rem"},"props":{"variant_2":"filled","_showFormHelperText":false,"fullWidth":true},"imports":{"mui":["FormControl"]},"parentId":"2b277970-52bc-4f9b-84a5-1c6140907ebd"},{"id":"709db6f2-0298-46da-b22c-dbe21c5754bf","name":"FilledInput","allowChildren":false,"styles":{},"props":{"type":"text"},"imports":{"mui":["FilledInput"]},"parentId":"891e0162-bc7b-47e8-afd1-f2ea90e8075c","deleteWithParent":true},{"id":"7bd889cb-9248-467e-bdb2-10dd08ad85c9","name":"InputLabel","allowChildren":false,"styles":{},"props":{"text":"Username"},"imports":{"mui":["InputLabel"]},"parentId":"891e0162-bc7b-47e8-afd1-f2ea90e8075c","deleteWithParent":true},{"id":"8989af2c-72c6-41e7-b2d8-f15738bc49c5","name":"FormControl","allowChildren":true,"styles":{"margin":""},"props":{"variant_2":"filled","_showFormHelperText":false,"fullWidth":true},"imports":{"mui":["FormControl"]},"parentId":"2b277970-52bc-4f9b-84a5-1c6140907ebd"},{"id":"4bbaab7e-0f86-4f2c-a183-6709299f30dc","name":"FilledInput","allowChildren":false,"styles":{},"props":{"type":"password"},"imports":{"mui":["FilledInput"]},"parentId":"8989af2c-72c6-41e7-b2d8-f15738bc49c5","deleteWithParent":true},{"id":"f8b944cc-c5fe-4d84-a1cf-2a334e981abc","name":"InputLabel","allowChildren":false,"styles":{},"props":{"text":"Password"},"imports":{"mui":["InputLabel"]},"parentId":"8989af2c-72c6-41e7-b2d8-f15738bc49c5","deleteWithParent":true},{"id":"5acfeb41-7243-4662-b9ff-5d08d3c9e8b9","name":"Link","allowChildren":false,"styles":{"color":"#1976D2","margin":"","padding":"","fontWeight":"","fontSize":"","display":"flex","alignItems":"","justifyContent":"end"},"props":{"text":"Forgot password?","underline":"hover"},"imports":{"mui":["Link"]},"parentId":"2b277970-52bc-4f9b-84a5-1c6140907ebd"},{"id":"409b532e-f541-4ab2-b064-98a61558bc6b","name":"Button","allowChildren":false,"styles":{"color":"","backgroundColor":"","hover":{"backgroundColor":""},"margin":"1rem 0","padding":""},"props":{"text":"Login","variant_1":"contained","size":"medium","fullWidth":true},"imports":{"mui":["Button"]},"parentId":"2b277970-52bc-4f9b-84a5-1c6140907ebd"},{"id":"ee7138c7-7e95-454a-8afa-e855cc608fb2","name":"Typography","allowChildren":false,"styles":{"color":"#1976D2","textAlign":"center","margin":"","padding":"","fontWeight":"600","fontSize":""},"props":{"text":"Or Sign Up Using"},"imports":{"mui":["Typography"]},"parentId":"2b277970-52bc-4f9b-84a5-1c6140907ebd"},{"id":"ced3086c-16c1-4838-94aa-8e4f04427574","name":"Box","allowChildren":true,"styles":{"width":"100%","height":"auto","backgroundColor":"","margin":"0.5rem 0 0","padding":"","display":"flex","flexDirection":"row","gap":"0.5rem","alignItems":"center","justifyContent":"center"},"props":{},"imports":{"mui":["Box"]},"parentId":"2b277970-52bc-4f9b-84a5-1c6140907ebd"},{"id":"61248dc0-7438-4775-a3fb-f325704eec5a","name":"IconButton","allowChildren":true,"styles":{"color":"#FFFFFF","backgroundColor":"#008744","hover":{"backgroundColor":"#056135"},"margin":"","padding":""},"props":{"_iconName":"Google"},"imports":{"mui":["IconButton"]},"parentId":"ced3086c-16c1-4838-94aa-8e4f04427574"},{"id":"9c3e52d1-a3a5-447a-a3e0-e2b99a207132","name":"IconButton","allowChildren":true,"styles":{"color":"#FFFFFF","backgroundColor":"#3b5998","hover":{"backgroundColor":"#263c64"},"margin":"","padding":""},"props":{"_iconName":"Facebook"},"imports":{"mui":["IconButton"]},"parentId":"ced3086c-16c1-4838-94aa-8e4f04427574"},{"id":"98d45f49-4b45-42c3-890f-0115067f4c3c","name":"IconButton","allowChildren":true,"styles":{"color":"#FFFFFF","backgroundColor":"#000000","hover":{"backgroundColor":"#2c2b2b"},"margin":"","padding":""},"props":{"_iconName":"Apple"},"imports":{"mui":["IconButton"]},"parentId":"ced3086c-16c1-4838-94aa-8e4f04427574"},{"id":"935db2ee-aacb-454b-95a7-673dd9454868","name":"IconButton","allowChildren":true,"styles":{"color":"#FFFFFF","backgroundColor":"#00acee","hover":{"backgroundColor":"#0082b4"},"margin":"","padding":""},"props":{"_iconName":"Twitter"},"imports":{"mui":["IconButton"]},"parentId":"ced3086c-16c1-4838-94aa-8e4f04427574"},{"id":"e22c17df-3cf6-4c35-b037-c5907b29ca88","component":{"type":{},"compare":null},"name":"Google","allowChildren":false,"styles":{},"props":{},"imports":{"muiIcons":["Google"]},"parentId":"61248dc0-7438-4775-a3fb-f325704eec5a","deleteWithParent":true},{"id":"4a8a210e-4691-4027-9648-8c432db69222","component":{"type":{},"compare":null},"name":"Facebook","allowChildren":false,"styles":{},"props":{},"imports":{"muiIcons":["Facebook"]},"parentId":"9c3e52d1-a3a5-447a-a3e0-e2b99a207132","deleteWithParent":true},{"id":"a65e85f1-be0c-43bb-a070-99bf5cd9b07b","component":{"type":{},"compare":null},"name":"Apple","allowChildren":false,"styles":{},"props":{},"imports":{"muiIcons":["Apple"]},"parentId":"98d45f49-4b45-42c3-890f-0115067f4c3c","deleteWithParent":true},{"id":"7a7c6358-b714-4e68-b2f7-d52f91952560","component":{"type":{},"compare":null},"name":"Twitter","allowChildren":false,"styles":{},"props":{},"imports":{"muiIcons":["Twitter"]},"parentId":"935db2ee-aacb-454b-95a7-673dd9454868","deleteWithParent":true}]';

const ComponentsDesigner = (props) => {
    const {
        isPreview
    } = props;

    const {
        components,
        handleDrop,
        allowDrop,
        makeComponent,
        loadDemo,
    } = useComponents();

    useEffect(() => {
        !isPreview && loadDemo(demoJSON);
    }, []);

    return (
        <div
            className={`${isPreview ? 'components-designer-preview' : 'components-designer'}`}
            onDrop={(event) => handleDrop(event)}
            onDragOver={(event) => allowDrop(event)}
        >
            {
                components.filter(x => x.parentId === undefined).map((comp, index) => 
                    <React.Fragment
                        key={index}
                    >
                        {makeComponent(comp)}
                    </React.Fragment>
                ) 
            }
        </div>
    );
};

export default ComponentsDesigner;