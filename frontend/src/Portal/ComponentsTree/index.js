import React, { useEffect } from "react";
import useComponents from "../../ComponentsContext/useComponents";
import { TreeItem, TreeView } from "@mui/x-tree-view";
import { ChevronRight, ExpandMore } from "@mui/icons-material";

const ComponentsTree = () => {
    const [expanded, setExpanded] = React.useState([]);
    const [selected, setSelected] = React.useState('');

    const {
        componentsTree,
        handleSetSelectedComponent,
        selectedComponent,
        components
    } = useComponents();

    useEffect(() => {
        selectedComponent && setSelected(selectedComponent.id);
        selectedComponent && setExpanded(handleParentExpand(selectedComponent, [...expanded]));
    }, [selectedComponent]);

    const makeNode = (comp) => {
        
        if(comp.children && comp.children.length > 0){
            return (
                <TreeItem
                    nodeId={comp.id}
                    label={comp.name}
                    key={comp.id}
                >
                    {
                        comp.children.map((comp) =>
                            makeNode(comp)
                        )
                    }
                </TreeItem>
            );
        }else{
            return (
                <TreeItem 
                    nodeId={comp.id}
                    label={comp.name}
                    key={comp.id}
                />
            );
        }

        
    };

    const handleSelect = (event, nodeId) => {
        setSelected(nodeId);
        handleSetSelectedComponent(event, nodeId)
    };

    const handleToggle = (event, nodeIds) => {
        setExpanded(nodeIds);
    };

    const handleParentExpand = (comp, array) => {
        let newArray = array || [];
        if (!newArray.includes(comp.id)) {
            newArray.push(comp.id)
            if(comp.parentId){
                newArray = handleParentExpand(components.find(c => c.id === comp.parentId), array);
            }
        }
        return newArray;
    };

    return (
        <React.Fragment>
            <div className='components-menu-header'>Components tree</div>
            
            <TreeView
                defaultCollapseIcon={<ExpandMore />}
                defaultExpandIcon={<ChevronRight />}
                onNodeSelect={handleSelect}
                expanded={expanded}
                onNodeToggle={handleToggle}
                selected={selected}
            >
                {
                    componentsTree.length > 0 && componentsTree.map((comp) => 
                        makeNode(comp)
                    )
                }  
            </TreeView>
        </React.Fragment>
    );
};

export default ComponentsTree;