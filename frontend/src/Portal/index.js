import React from 'react';
import CodeGenerator from './CodeGenerator';
import ComponentsDesigner from './ComponentsDesigner';
import ComponentSettings from './ComponentSettings';
import ComponentsMenu from './ComponentsMenu';

import './index.css';
import PortalHeader from './PortalHeader';
import { Tab, Tabs, Tooltip } from '@mui/material';
import { GridView, Segment } from '@mui/icons-material';
import { useState } from 'react';
import ComponentsTree from './ComponentsTree';

const TabPanel = (props) => {
    const {
        children,
        index,
        value
    } = props;

    return (
        <div
            role='tabpanel'
            hidden={value !== index}
        >
            {
                children
            }
        </div>
    );
};

const Portal = () => {

    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className='portal'>
            <div className='portal-header'>
                <PortalHeader />
            </div>
            <div className='portal-content'>
                <div className='components-menu'>
                <Tabs
                        variant="fullWidth"
                        value={value}
                        onChange={handleChange}
                        sx={{
                            '& .MuiTabs-indicator': {
                                backgroundColor: '#408386'
                            },
                            '& .Mui-selected': {
                                color: '#408386 !important'
                            }
                        }}
                    >
                        <Tab
                            icon={
                                <Tooltip title='Components'>
                                    <GridView />
                                </Tooltip>
                            }
                            sx={{
                                color: 'inherit'
                            }}
                        />
                        <Tab
                            icon={
                                <Tooltip title='Components Tree'>
                                    <Segment />
                                </Tooltip>
                            }
                            sx={{
                                color: 'inherit'
                            }}
                        />
                    </Tabs>
                    <React.Fragment>
                        <TabPanel value={value} index={0}>
                            <ComponentsMenu />
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            <ComponentsTree />
                        </TabPanel>
                    </React.Fragment>
                </div>
                <div className='content-container'>
                    <ComponentsDesigner />
                    <CodeGenerator />
                </div>
                <div className='coponents-settings'>
                    <ComponentSettings />
                </div>
            </div>
            <div className='portal-footer'>

            </div>
        </div>
    );
};

export default Portal;