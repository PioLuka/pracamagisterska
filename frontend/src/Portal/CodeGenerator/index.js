import React, { useEffect, useState } from "react";
import Prism from 'prismjs';
//import './prism.css';
import './prism-light.css';

import parserBabel from "prettier/parser-babel.js";
import cssParser from 'prettier/parser-postcss'
import prettier from "prettier";

import { Button, Switch, Tab, Tabs } from "@mui/material";
import useComponents from "../../ComponentsContext/useComponents";

const TabPanel = (props) => {
    const {
        children,
        index,
        value
    } = props;

    return (
        <div
            role='tabpanel'
            hidden={value !== index}
        >
            {
                children
            }
        </div>
    );
};

const CodeGenerator = () => {

    const [isCopied, setCopied] = useState(false);
    const [selectedTab, setSelectedTab] = React.useState(0);

    const {
        reactCode,
        cssCode,
        setIsCssEnabled,
        isCssEnabled
    } = useComponents();

    const handleChange = (event, newValue) => {
        setSelectedTab(newValue);
    };

    const formatReactCode = () => {
        try {
            return prettier.format(reactCode.trim(), {
                parser: "babel",
                plugins: [parserBabel],
            });
        } catch (error) {
            console.log(error);
        }
    };

    const formatCSSCode = () => {
        try {
            return prettier.format(cssCode.trim(), {
                parser: "css",
                plugins: [cssParser],
            });
            
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        Prism.highlightAll();
    }, [reactCode, cssCode]);

    const saveCodeToClipboard = () => {
        setCopied(true);
        navigator.clipboard.writeText(selectedTab === 0 ? formatReactCode(reactCode) : formatCSSCode(cssCode));

        setTimeout(() => { setCopied(false)}, 3000);
    };

    const handleCssSwitch = (event) => {
        if(!event.target.checked){
            setSelectedTab(0);
        }

        setIsCssEnabled(event.target.checked);
    };

    return (
        <div className="code-generator">
            <div className="code-generator-menu">
                <Tabs 
                    value={selectedTab} 
                    onChange={handleChange}
                    sx={{
                        '& .MuiTabs-indicator': {
                            backgroundColor: '#408386'
                        },
                        '& .Mui-selected': {
                            color: '#408386 !important'
                        }
                    }}
                >
                    <Tab 
                        label='React code' 
                        sx={{
                            color: 'inherit'
                        }}
                    />
                    {
                        isCssEnabled &&
                            <Tab 
                                label='CSS code'
                                sx={{
                                    color: 'inherit'
                                }}
                            />
                    }
                    
                </Tabs>
                <div className="code-generator-css-switch">
                    Enable CSS
                    <Switch
                        value={isCssEnabled}
                        onChange={handleCssSwitch}
                        sx={{
                            '& .MuiSwitch-switchBase.Mui-checked': {
                                color: '#408386'
                            },
                            '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
                                backgroundColor: '#408386',
                            },
                        }}
                    />
                </div>
                
                <Button
                    sx={{
                        // position: 'absolute',
                        // top: 0,
                        // right: 0,
                        // margin: '1rem',
                        color: '#cccccc',
                        backgroundColor: '#408386',
                        borderColor: '#67cdcc',
                        '&:hover': {
                            borderColor: '#408386',
                            backgroundColor: '#408386',
                            opacity: '70%'
                        },
                        fontSize: '0.75rem',
                        fontWeight: 600,
                        width: '4.5rem',
                        margin: '0 1rem'
                    }}
                    onClick={saveCodeToClipboard}
                    variant='outlined'
                    size="small"
                >
                    {!isCopied ? 'Copy' : 'Copied'}
                </Button>
            </div>
            <div className='code-generator-code'>
                <TabPanel value={selectedTab} index={0}>
                    <pre>
                        <code className={`language-javascript`}>
                            {formatReactCode()}
                        </code>
                    </pre>
                </TabPanel>
                <TabPanel value={selectedTab} index={1}>
                    <pre>
                        <code className={`language-css`}>
                            {formatCSSCode()}
                        </code>
                    </pre>
                </TabPanel>
            </div>
        </div>
        
    );
};

CodeGenerator.propTypes = {};


export default CodeGenerator;