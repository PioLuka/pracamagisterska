import React from "react";
import {
  Box,
  Container,
  Typography,
  FormControl,
  FilledInput,
  InputLabel,
  Link,
  Button,
  IconButton,
} from "@mui/material";
import { Google, Facebook, Apple, Twitter } from "@mui/icons-material";
import "./Template.css";

const Template = () => {
  return (
    <>
      <Box className="Box_b524a55f5fcb42a3965aab2ea18b8a64">
        <Container
          maxWidth="xs"
          className="Container_cd7f0d1e808d4886a38e0bde9388a8d9"
        >
          <Box className="Box_2b27797052bc4f9b84a51c6140907ebd">
            <Typography className="Typography_af6cd04c005b49ceadf0110b65c97615">
              Login
            </Typography>
            <FormControl
              variant="filled"
              fullWidth="true"
              className="FormControl_891e0162bc7b47e8afd1f2ea90e8075c"
            >
              <FilledInput
                type="text"
                className="FilledInput_709db6f2029846dab22cdbe21c5754bf"
              />
              <InputLabel className="InputLabel_7bd889cb9248467ebdb210dd08ad85c9">
                Username
              </InputLabel>
            </FormControl>
            <FormControl
              variant="filled"
              fullWidth="true"
              className="FormControl_8989af2c72c641e7b2d8f15738bc49c5"
            >
              <FilledInput
                type="password"
                className="FilledInput_4bbaab7e0f864f2ca1836709299f30dc"
              />
              <InputLabel className="InputLabel_f8b944ccc5fe4d84a1cf2a334e981abc">
                Password
              </InputLabel>
            </FormControl>
            <Link
              underline="hover"
              className="Link_5acfeb4172434662b9ff5d08d3c9e8b9"
            >
              Forgot password?
            </Link>
            <Button
              variant="contained"
              size="medium"
              fullWidth="true"
              className="Button_409b532ef5414ab2b06498a61558bc6b"
            >
              Login
            </Button>
            <Typography className="Typography_ee7138c77e95454a8afae855cc608fb2">
              Or Sign Up Using
            </Typography>
            <Box className="Box_ced3086c16c1483894aa8e4f04427574">
              <IconButton className="IconButton_61248dc074384775a3fbf325704eec5a">
                <Google className="Google_e22c17df3cf64c35b037c5907b29ca88" />
              </IconButton>
              <IconButton className="IconButton_9c3e52d1a3a5447aa3e0e2b99a207132">
                <Facebook className="Facebook_4a8a210e4691402796488c432db69222" />
              </IconButton>
              <IconButton className="IconButton_98d45f494b4542c3890f0115067f4c3c">
                <Apple className="Apple_a65e85f1be0c43bba07099bf5cd9b07b" />
              </IconButton>
              <IconButton className="IconButton_935db2eeaacb454b95a7673dd9454868">
                <Twitter className="Twitter_7a7c6358b7144e68b2f7d52f91952560" />
              </IconButton>
            </Box>
          </Box>
        </Container>
      </Box>
    </>
  );
};

export default Template;
