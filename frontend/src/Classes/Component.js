import { uuidv4 } from "../Utils/guid";

class Component {
    constructor(
        component,
        name,
        allowChildren,
        styles,
        props,
        imports,
        parentId,
        deleteWithParent,
        id
    ) {
        this.id = id || uuidv4();
        this.component = component;
        this.name = name;
        this.allowChildren = allowChildren;
        this.styles = Object.fromEntries(
            Object.entries(styles)
                .map(([key, val]) => [key, val])
        );
        this.props = Object.fromEntries(
            Object.entries(props)
                .map(([key, val]) => [key, val])
        );
        this.imports = Object.fromEntries(
            Object.entries(imports)
                .map(([key, val]) => [key, val])
        );
        this.parentId = parentId;

        for (const [key, value] of Object.entries(styles)) {
            if (typeof value === 'object') {
                this.styles[key] = Object.fromEntries(
                    Object.entries(this.styles[key])
                        .map(([key, val]) => [key, val])
                );
            }
        }
        this.deleteWithParent = deleteWithParent;
    }
}

export default Component;