import React from "react";

const Div = (props) => {
    const {
        children,
        backgroundColor,
        display,
        width,
        height
    } = props;

    const divStyle = {
        backgroundColor: backgroundColor,
        display: display,
        width,
        height
    };

    return(
        <div style={divStyle}>{children}</div>
    );
};

export default Div;