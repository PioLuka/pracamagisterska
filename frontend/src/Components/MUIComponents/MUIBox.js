import React, { useEffect, useState } from 'react';
import {Box} from '@mui/material';
import useComponents from '../../ComponentsContext/useComponents';

const MUIBox = (properties) => {
    const {
        //width,
        //height,
        //backgroundColor,
        id,
        onClick,
        onDragStart,
        draggable,
        props,
        styles,

        classes
    } = properties;

    const {
        handleDrop,
        allowDrop,
        makeComponent,
        components,
        selectedComponent
    } = useComponents();

    const [children, setChildren] = useState([]);

    useEffect(() => {
        setChildren(components.filter(x => x.parentId === id));
    }, [components]);

    return (
        <Box
            sx={{
                width: styles.width,
                height: styles.height,
                backgroundColor: styles.backgroundColor,
                marginTop: styles.marginTop,
                marginRight: styles.marginRight,
                marginBottom: styles.marginBottom,
                marginLeft: styles.marginLeft,
                margin: styles.margin,
                padding: styles.padding,
                display: styles.display,
                flexDirection: styles.flexDirection,
                gap: styles.gap,
                justifyContent: styles.justifyContent,
                alignItems: styles.alignItems
            }}

            onDrop={(event) => handleDrop(event, id)}
            onDragOver={(event) => allowDrop(event)}
            //className={`${selectedComponent?.id === id ? 'selected-item-border' : 'not-selected-item-border'}`}
            className={classes}
            onClick={onClick}
            draggable={draggable}
            onDragStart={onDragStart}
        >
            {
                children.map((child, index) => {
                    return (
                        <React.Fragment key={index}>
                            {makeComponent(child)}
                        </React.Fragment>
                    );
                })
            }
        </Box>
    );
};

export default MUIBox;