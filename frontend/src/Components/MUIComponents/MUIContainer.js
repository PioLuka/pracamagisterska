import { Container } from "@mui/system";
import React, { useEffect, useState } from "react";
import useComponents from "../../ComponentsContext/useComponents";

const MUIContainer = (properties) => {
    const {
        id,
        onDragStart,
        draggable,
        onClick,
        props,
        styles,

        classes
    } = properties;

    const {
        handleDrop,
        allowDrop,
        makeComponent,
        components,
        selectedComponent
    } = useComponents();

    const [children, setChildren] = useState([]);

    useEffect(() => {
        setChildren(components.filter(x => x.parentId === id));
    }, [components]);

    return (
        <Container
            maxWidth={props.maxWidth}

            onDrop={(event) => handleDrop(event, id)}
            onDragOver={(event) => allowDrop(event)}
            //className={`${selectedComponent?.id === id ? 'selected-item-border' : 'not-selected-item-border'}`}
            className={classes}
            onClick={onClick}
            draggable={draggable}
            onDragStart={onDragStart}
            sx={{
                height: children.length > 0 ? 'auto' : '100px',
                margin: styles.margin,
                padding: styles.padding
            }}
        >
            {
                children.map((child, index) => {
                    return (
                        <React.Fragment key={index}>
                            {makeComponent(child)}
                        </React.Fragment>
                    );
                })
            }
        </Container>
    );
};

export default MUIContainer;
