import { Link } from "@mui/material";
import React from "react";

const MUILink = (properties) => {
    const {
        props,
        styles,

        onClick,
        onDragStart,
        draggable,
        classes
    } = properties;

    return (
        <Link
            onClick={onClick}
            className={classes}
            draggable={draggable}
            onDragStart={onDragStart}

            underline={props.underline}
            sx={{
                color: styles.color,
                margin: styles.margin,
                padding: styles.padding,
                cursor: 'pointer',
                fontSize: styles.fontSize,
                fontWeight: styles.fontWeight,
                display: styles.display,
                justifyContent: styles.justifyContent,
                alignItems: styles.alignItems
            }}
        >
            {props.text}
        </Link>
    );
};

export default MUILink;