import React from 'react';
import PropTypes from 'prop-types';
import { Button, styled } from '@mui/material';
import useComponents from '../../ComponentsContext/useComponents';

const MUIButton = (properties) => {
    const {
        onClick,
        id,
        onDragStart,
        draggable,
        props,
        styles,

        classes
    } = properties;

    const {
        selectedComponent
    } = useComponents();

    const CustomButton = styled(Button)({
        color: styles.color,
        backgroundColor: styles.backgroundColor,
        '&:hover': {
            backgroundColor: styles.hover.backgroundColor
        },
        margin: styles.margin,
        padding: styles.padding
    });

    return (
        <CustomButton 
            variant={props.variant_1}
            size={props.size}

            //className={`${selectedComponent?.id === id ? 'selected-item-border' : 'not-selected-item-border'}`}
            className={classes}
            onClick={onClick}
            draggable={draggable}
            onDragStart={onDragStart}
            fullWidth={props.fullWidth}
        >
            {props.text}
        </CustomButton>
    );
};

Button.propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func
};

Button.defaultProps  = {
    //text: 'Button'
};

export default MUIButton;