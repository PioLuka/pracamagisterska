import { Typography } from "@mui/material";
import React from "react";
import useComponents from "../../ComponentsContext/useComponents";

const MUITypography = (properties) => {
    const {
        id,
        children,
        onClick,
        onDragStart,
        draggable,
        props,
        styles,

        classes
    } = properties;

    const {
        selectedComponent
    } = useComponents();

    return (
        <Typography
            //className={`${selectedComponent?.id === id ? 'selected-item-border' : 'not-selected-item-border'}`}
            className={classes}
            onClick={onClick}
            draggable={draggable}
            onDragStart={onDragStart}

            sx={{
                color: styles.color,
                textAlign: styles.textAlign,
                margin: styles.margin,
                padding: styles.padding,
                fontSize: styles.fontSize,
                fontWeight: styles.fontWeight
            }}
        >
            {props.text}
        </Typography>
    );
};

export default MUITypography;