import { Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import Component from "../../Classes/Component";
import useComponents from "../../ComponentsContext/useComponents";
import { MUIComponents } from "../MUIComponents";
import MUIGridItem from "./MUIGridItem";

const MUIGrid = (properties) => {
    const {
        //spacing,
        //direction,
        //justifyContent,
        //alignItems,
        id,
        onClick,
        onDragStart,
        draggable,
        props,
        styles,

        classes
    } = properties;

    const {
        handleDrop,
        allowDrop,
        components,
        selectedComponent
    } = useComponents();

    const [children, setChildren] = useState([]);

    useEffect(() => {
        // const comps = components.filter(c => c.parentId === id);
        // if(comps.length === 0) {
        //     for(let count = 0; count < itemsCount; count++){
        //         handleAdd(createGridItem());
        //     }
        // }
        setChildren(components.filter(c => c.parentId === id));
    }, [components.length]);

    // useEffect(() => {
    //     console.log(itemsCount)
    //     console.log(prevItemsCount)
    //     if(prevItemsCount > itemsCount) {
    //         console.log(children)
    //         const childrenCopy = [...children].reverse();

    //         for(let count = 0; count < prevItemsCount - itemsCount; count++){
    //             console.log(ch)
    //         }

    //         console.log(childrenCopy.reverse())
    //     } else {

    //     }
    //     //setPrevItemsCount(itemsCount);
    // }, [itemsCount]);

    const createGridItem = (event) => {
        const muiGridItem = MUIComponents.find(x => x.name === 'GridItem');
        const newGridItem = new Component(
            muiGridItem.component, 
            muiGridItem.name,
            muiGridItem.allowChildren,
            muiGridItem.styles,
            muiGridItem.props,
            muiGridItem.imports,
            id,
            muiGridItem.deleteWithParent
        );

        // handleAdd(newGridItem);
        // console.log(newGridItem)
        handleDrop(event, newGridItem.id, newGridItem);
    };

    return(
        <Grid 
            container
            spacing={props.spacing}
            direction={props.direction || ''}
            justifyContent={props.justifyContent}
            alignItems={props.alignItems}
            //sx={{width: `${children.length > 0 ? 'auto' : width}`, height: `${children.length > 0 ? 'auto' : height}`}}
            // onDrop={(event) => handleDrop(event, id)}
            // onDragOver={(event) => allowDrop(event)}
            sx={{height: children.length > 0 ? 'auto' : '100px'}}

            onDrop={(event) => createGridItem(event)}
            onDragOver={(event) => allowDrop(event)}
            //className={`${selectedComponent?.id === id ? 'selected-item-border' : 'not-selected-item-border'}`}
            className={classes}
            onClick={onClick}
            draggable={draggable}
            onDragStart={onDragStart}
        >
            {/* <Grid item>TEST</Grid>

            <Grid item>TEST2</Grid> */}
            {
                // children.map((child, index) => {
                //     return(
                //         <Grid key={index} item>
                //             {makeComponent(child)}
                //         </Grid>
                //     );
                // })

                // Array.from(Array(itemsCount)).map((item, index) => {
                //     return (
                //         <MUIGridItem key={index}/>
                //     );
                // })

                children.map((child, index) => {
                    return(
                        <child.component id={child.id} key={index}/>
                    );
                })
            }
        </Grid>
    );
};

export default MUIGrid;