import {Apple, Facebook, Google, Twitter} from '@mui/icons-material';

const MUIIconsRegister = [
    {
        name: 'Google',
        component: Google,
        imports: {
            muiIcons: [
                'Google'
            ]
        }
    },
    {
        name: 'Facebook',
        component: Facebook,
        imports: {
            muiIcons: [
                'Facebook'
            ]
        }
    },
    {
        name: 'Apple',
        component: Apple,
        imports: {
            muiIcons: [
                'Apple'
            ]
        }
    },
    {
        name: 'Twitter',
        component: Twitter,
        imports: {
            muiIcons: [
                'Twitter'
            ]
        }
    }
];

export default MUIIconsRegister;