import { FormHelperText, OutlinedInput } from "@mui/material";
import React from "react";

const MUIFormHelperText = (properties) => {
    const {
        props,

        classes
    } = properties;

    return (
        <FormHelperText
            className={classes}
        > 
            {props.text}
        </FormHelperText>
    );
};

export default MUIFormHelperText;
