import { Input } from "@mui/material";
import React from "react";

const MUIInput = (properties) => {
    const {
        props,

        classes
    } = properties;

    return (
        <Input 
            className={classes}
            type={props.type}
        />
    );
};

export default MUIInput;
