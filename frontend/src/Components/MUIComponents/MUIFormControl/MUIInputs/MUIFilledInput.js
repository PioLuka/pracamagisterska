import { FilledInput } from "@mui/material";
import React from "react";

const MUIFilledInput = (properties) => {
    const {
        props,
        parentId,
        id,

        classes
    } = properties;

    return (
        <FilledInput 
            className={classes}
            type={props.type}
        />
    );
};

export default MUIFilledInput;