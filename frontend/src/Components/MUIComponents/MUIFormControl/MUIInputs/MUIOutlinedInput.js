import { OutlinedInput } from "@mui/material";
import React from "react";

const MUIOutlinedInput = (properties) => {
    const {
        props,

        classes
    } = properties;

    return (
        <OutlinedInput
            className={classes}
            type={props.type}
        />
    );
};

export default MUIOutlinedInput;
