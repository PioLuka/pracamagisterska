import { InputLabel, OutlinedInput } from "@mui/material";
import React from "react";

const MUIInputLabel = (properties) => {
    const {
        props,

        classes
    } = properties;

    return (
        <InputLabel className={classes}>
            {props.text}
        </InputLabel>
    );
};

export default MUIInputLabel;
