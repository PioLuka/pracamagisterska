import { FormControl, formControlClasses, InputLabel } from "@mui/material";
import React, { useEffect, useState } from "react";
import Component from "../../../Classes/Component";
import useComponents from "../../../ComponentsContext/useComponents";
import { MUIComponents } from "../../MUIComponents";
import MUIFormHelperText from "./MUIFormHelperText";
import MUIFilledInput from "./MUIInputs/MUIFilledInput";
import MUIInput from "./MUIInputs/MUIInput";
import MUIOutlinedInput from "./MUIInputs/MUIOutlinedInput";

const MUIFormControl = (properties) => {
    const {
        id,
        props,
        styles,

        onClick,
        onDragStart,
        draggable,
        classes
    } = properties;

    const {
        selectedComponent,
        handleAdd,
        handleUpdate,
        components,
        makeComponent,
        handleRemove
    } = useComponents();

    const [inputComponent, setInputComponent] = useState(components.find(x => x.parentId === id && x.name.includes('Input')));
    const [formHelperText, setFormHelperText] = useState(components.find(x => x.parentId === id && x.name === 'FormHelperText'));
    const [inputLabel, setInputLabel] = useState(components.find(x => x.parentId === id && x.name === 'InputLabel'));

    // const inputComponent = components.find(x => x.id === inputComponent.id);
    // const inputComponent = components.find(x => x.id === inputComponent.id);

    useEffect(() => {
        switch (props.variant_2) {
            case 'standard':
                changeInputType('Input', MUIInput);
                break;
            case 'outlined':
                changeInputType('OutlinedInput', MUIOutlinedInput);
                break;
            case 'filled':
                changeInputType('FilledInput', MUIFilledInput);
                break;
        }
    }, [props.variant_2]);

    useEffect(() => {
        handleShowFormHelperText();
    }, [props._showFormHelperText]);

    useEffect(() => {
        handleCreateInputLabel();
    }, [])

    const changeInputType = (name, componentBase) => {
        const currInputType = components.find(x => x.parentId === id && x.name.includes('Input'));
        const muiInput = MUIComponents.find(x => x.name === name);
        if(inputComponent || currInputType){
            //const currInputType = components.find(x => x.id === inputComponent.id);
            currInputType.name = name;
            currInputType.component = componentBase;
            currInputType.imports = muiInput.imports;
            handleUpdate(currInputType.id, currInputType);
            setInputComponent(currInputType);
        }
        
        if(!inputComponent && !currInputType){
            const newInput = new Component(
                muiInput.component, 
                muiInput.name,
                muiInput.allowChildren,
                muiInput.styles,
                muiInput.props,
                muiInput.imports,
                id,
                muiInput.deleteWithParent
            );
            handleAdd(newInput);
            setInputComponent(newInput);
        }
    };

    const handleShowFormHelperText = () => {
        const currFormHelperText = components.find(x => x.parentId === id && x.name === 'FormHelperText');
        if(!props._showFormHelperText && currFormHelperText){
            handleRemove(currFormHelperText.id);
            setFormHelperText(undefined);
        }
        
        if(props._showFormHelperText && !currFormHelperText){
            const formHelperText = MUIComponents.find(x => x.name === 'FormHelperText');
            const newFormHelperText = new Component(
                formHelperText.component, 
                formHelperText.name,
                formHelperText.allowChildren,
                formHelperText.styles,
                formHelperText.props,
                formHelperText.imports,
                id,
                formHelperText.deleteWithParent
            );
            handleAdd(newFormHelperText);
            setFormHelperText(newFormHelperText);
        }
    };

    const handleCreateInputLabel = () => {
        const currFormHelperText = components.find(x => x.parentId === id && x.name === 'InputLabel');
        if(!currFormHelperText){
            const inputLabel = MUIComponents.find(x => x.name === 'InputLabel');
            const newInputLabel = new Component(
                inputLabel.component, 
                inputLabel.name,
                inputLabel.allowChildren,
                inputLabel.styles,
                inputLabel.props,
                inputLabel.imports,
                id,
                inputLabel.deleteWithParent
            );
            handleAdd(newInputLabel);
            setInputLabel(newInputLabel);
        }
    };

    return (
        <FormControl
            onClick={onClick}
            //className={`${selectedComponent?.id === id ? 'selected-item-border' : 'not-selected-item-border'}`}
            className={classes}
            draggable={draggable}
            onDragStart={onDragStart}

            variant={props.variant_2}
            sx={{
                margin: styles.margin
            }}
            fullWidth={props.fullWidth}
        >
            {inputLabel && makeComponent(inputLabel)}
            {inputComponent && makeComponent(inputComponent)}
            {formHelperText && makeComponent(formHelperText)}
        </FormControl>
    );
};

export default MUIFormControl;
