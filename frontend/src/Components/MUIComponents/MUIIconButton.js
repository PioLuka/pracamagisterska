import { IconButton } from "@mui/material";
import React, { useEffect, useState } from "react";
import Component from "../../Classes/Component";
import useComponents from "../../ComponentsContext/useComponents";
import MUIIconsRegister from "./MUIIconsRegister";

const MUIIconButton = (properties) => {
    const {
        id,
        name,
        props,
        styles,

        onClick,
        onDragStart,
        draggable,
        classes
    } = properties;

    const {
        makeComponent,
        handleAdd,
        components
    } = useComponents();

    const [iconComponent, setIconComponent] = useState();

    useEffect(() => {
        makeIcon(props._iconName);
    }, [props._iconName]);

    const makeIcon = (iconName) => {
        const currIcon = components.find(x => x.parentId === id);
        if(!iconComponent && iconName !== '' && !currIcon){
            const muiIcon = MUIIconsRegister.find(x => x.name === iconName);
            const newIcon = new Component(
                muiIcon.component, 
                muiIcon.name,
                false,
                {},
                {},
                muiIcon.imports,
                id,
                true
            );
            handleAdd(newIcon);
            setIconComponent(newIcon);
        }

        if(!iconComponent && iconName !== '' && currIcon){
            setIconComponent(currIcon);
        }
    };

    return (
        <IconButton
            sx={{
                color: styles.color,
                backgroundColor: styles.backgroundColor,
                '&:hover': {
                    backgroundColor: styles.hover.backgroundColor
                },
                margin: styles.margin,
                padding: styles.padding
            }}
            className={classes}
            onClick={onClick}
            draggable={draggable}
            onDragStart={onDragStart}
        >
            {
                iconComponent && makeComponent(iconComponent, true)
            }
        </IconButton>
    );
};

export default MUIIconButton;
