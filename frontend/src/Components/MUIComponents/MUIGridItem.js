import React, { useEffect, useState } from "react";
import { Grid, Paper } from "@mui/material";
import styled from "@emotion/styled";
import useComponents from "../../ComponentsContext/useComponents";

const Placeholder = styled(Paper)(({ theme }) => ({
    backgroundColor: '#679267',
    padding: '16px',
    textAlign: 'center',
    color: 'white',
    textOverflow: 'ellipsis'
}));

const MUIGridItem = (properties) => {
    const {
        id,
        props,

        classes
    } = properties;

    const {
        handleSetSelectedComponent,
        components,
        makeComponent,
        selectedComponent,
        allowDrop,
        handleDrop,
        handleRemove
    } = useComponents();

    const [child, setChild] = useState(undefined);

    useEffect(() => {
        const childCheck = components.find(c => c.parentId === id);
        
        if(childCheck){
            setChild(childCheck);
        }else{
            handleRemove(id);
        }
    }, [components]);

    const onDragOver = (event) => {
        event.stopPropagation();
        if(child) {
            return;
        }

        allowDrop(event);
    };

    const onDrop = (event) => {
        event.stopPropagation();
        if(child) {
            return;
        }

        handleDrop(event, id);
    };

    return (
        <Grid 
            item 
            xs={4}
            md={4}
            onClick={(event) => handleSetSelectedComponent(event, id)}
            className={classes}
            //className={`${selectedComponent?.id === id ? 'selected-item-border' : 'not-selected-item-border'}`}
            //onDragOver={(event) => onDragOver(event)}
            //onDrop={(event) => onDrop(event)}
            //sx={{height: child ? 'auto' : '100px'}}
        >
            {child && makeComponent(child)}
        </Grid>
    );
};

export default MUIGridItem;
