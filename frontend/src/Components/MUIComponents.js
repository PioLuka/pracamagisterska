import MUIButton from "./MUIComponents/MUIButton";
import MUIGrid from "./MUIComponents/MUIGrid";
import MuiBox from "./MUIComponents/MUIBox";
import MUIGridItem from "./MUIComponents/MUIGridItem";
import MUIContainer from "./MUIComponents/MUIContainer";
import MUITypography from "./MUIComponents/MUITypography";
import MUIFormControl from "./MUIComponents/MUIFormControl/MUIFormControl";
import MUIFilledInput from "./MUIComponents/MUIFormControl/MUIInputs/MUIFilledInput";
import MUIInput from "./MUIComponents/MUIFormControl/MUIInputs/MUIInput";
import MUIOutlinedInput from "./MUIComponents/MUIFormControl/MUIInputs/MUIOutlinedInput";
import MUIFormHelperText from "./MUIComponents/MUIFormControl/MUIFormHelperText";
import MUIInputLabel from "./MUIComponents/MUIFormControl/MUIInputLabel";
import MUIIconButton from "./MUIComponents/MUIIconButton";
import MUILink from "./MUIComponents/MUILink";

export const MUIComponents = [
    {
        id: '',
        component: MUIButton,
        name: 'Button',
        allowChildren: false,
        styles: {
            color: '',
            backgroundColor: '',
            hover: {
                backgroundColor: ''
            },
            margin: '',
            padding: ''
        },
        props: {
            text: 'Button',
            variant_1: '',
            size: '',
            fullWidth: false
        },
        imports: {
            mui: [
                'Button'
            ]
        }
    },
    {
        id: '',
        component: MUIGrid,
        name: 'Grid',
        allowChildren: true,
        styles: {
            // width: '100%',
            // height: '100%',
        },
        props: {
            spacing: 4,
            direction: '',
            justifyContent: '',
            alignItems: ''
        },
        imports: {
            mui: [
                'Grid'
            ]
        },
        defaults: {
            height: '50px',
            width: '100%'
        }
    },
    {
        id: '',
        component: MuiBox,
        name: 'Box',
        allowChildren: true,
        styles: {
            width: '100%',
            height: '100px',
            backgroundColor: '',
            margin: '',
            padding: '',
            display: '',
            flexDirection: '',
            gap: '',
            alignItems: '',
            justifyContent: ''
        },
        props: {
        },
        imports: {
            mui: [
                'Box'
            ]
        }
    },
    {
        id: '',
        component: MUIGridItem,
        name: 'GridItem',
        allowChildren: true,
        styles: {
            // width: '100%',
            // height: '100%',
        },
        props: {
            xs: 0,
            md: 0
        },
        imports: {
            mui: [
                'Grid'
            ]
        },
        code: false
    },
    {
        id: '',
        component: MUIContainer,
        name: 'Container',
        allowChildren: true,
        styles: {
            margin: '',
            padding: ''
        },
        props: {
            maxWidth: 'sm'
        },
        imports: {
            mui: [
                'Container'
            ]
        }
    },
    {
        id: '',
        component: MUITypography,
        name: 'Typography',
        allowChildren: false,
        styles: {
            color: '',
            textAlign: '',
            margin: '',
            padding: '',
            fontWeight: '',
            fontSize: ''
        },
        props: {
            text: 'Placeholder'
        },
        imports: {
            mui: [
                'Typography'
            ]
        }
    },
    {
        id: '',
        component: MUIFormControl,
        name: 'FormControl',
        allowChildren: true,
        styles: {
            margin: ''
        },
        props: {
            variant_2: 'standard',
            _showFormHelperText: false,
            fullWidth: ''
        },
        imports: {
            mui: [
                'FormControl'
            ]
        }
    },
    {
        id: '',
        component: MUIFilledInput,
        name: 'FilledInput',
        allowChildren: false,
        styles: {
            
        },
        props: {
            type: 'text'
        },
        imports: {
            mui: [
                'FilledInput'
            ]
        },
        deleteWithParent: true
    },
    {
        id: '',
        component: MUIInput,
        name: 'Input',
        allowChildren: false,
        styles: {
            
        },
        props: {
            type: 'text'
        },
        imports: {
            mui: [
                'Input'
            ]
        },
        deleteWithParent: true
    },
    {
        id: '',
        component: MUIOutlinedInput,
        name: 'OutlinedInput',
        allowChildren: false,
        styles: {
            
        },
        props: {
            type: 'text'
        },
        imports: {
            mui: [
                'OutlinedInput'
            ]
        },
        deleteWithParent: true
    },
    {
        id: '',
        component: MUIFormHelperText,
        name: 'FormHelperText',
        allowChildren: false,
        styles: {
            
        },
        props: {
            text: 'Form Helper Text'
        },
        imports: {
            mui: [
                'FormHelperText'
            ]
        },
        deleteWithParent: true
    },
    {
        id: '',
        component: MUIInputLabel,
        name: 'InputLabel',
        allowChildren: false,
        styles: {
            
        },
        props: {
            text: 'Label'
        },
        imports: {
            mui: [
                'InputLabel'
            ]
        },
        deleteWithParent: true
    },
    {
        id: '',
        component: MUIIconButton,
        name: 'IconButton',
        allowChildren: true,
        styles: {
            color: '',
            backgroundColor: '',
            hover: {
                backgroundColor: ''
            },
            margin: '',
            padding: ''
        },
        props: {
            _iconName: ''
        },
        imports: {
            mui: [
                'IconButton'
            ]
        }
    },
    {
        id: '',
        component: MUILink,
        name: 'Link',
        allowChildren: false,
        styles: {
            color: '',
            margin: '',
            padding: '',
            fontWeight: '',
            fontSize: '',
            display: '',
            alignItems: '',
            justifyContent: ''
        },
        props: {
            text: 'Link placeholder',
            underline: ''
        },
        imports: {
            mui: [
                'Link'
            ]
        }
    }
];
