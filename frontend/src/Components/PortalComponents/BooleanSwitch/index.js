import styled from "@emotion/styled";
import { FormControlLabel, Switch } from "@mui/material";
import React, { useEffect, useState } from "react";
import useComponents from "../../../ComponentsContext/useComponents";

const PortalSwitch = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: '#2ecc32'
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: '#2ecc32',
    },
  }));

const BooleanSwitch = (props) => {
    const {
        name,
        component
    } = props;

    const { changeProps } = useComponents();

    const [checked, setChecked] = useState(component.props[name]);

    useEffect(() => {
        setChecked(component.props[name]);
    }, [component]);

    const handleChange = (event) => {
        setChecked(event.target.checked);
        changeProps(component.id, name, event.target.checked);
    };

    return (
        <FormControlLabel 
            control={<PortalSwitch checked={checked || false} size='small'/>} 
            label={name}
            onChange={handleChange}
            labelPlacement='start'
        />
    );
};

export default BooleanSwitch;
