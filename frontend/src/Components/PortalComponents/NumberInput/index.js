import React, { useEffect, useState } from "react";
import {
    FormControl,
    InputLabel,
    TextField
} from "@mui/material";

import '../base.css';
import useComponents from "../../../ComponentsContext/useComponents";

const NumberInput = (props) => {
    const {
        name,
        component
    } = props;

    const [value, setValue] = useState(component.props[name]);

    const { changeProps } = useComponents();

    useEffect(() => {
        setValue(component.props[name]);
    }, [component]);

    const handleValueChange = (event) => {
        if (isNaN(event.target.value) || event.target.value < 0) {
            return;
        }

        setValue(event.target.value);
        changeProps(component.id, name, Number(event.target.value));
    }

    return (
        <div className="outlinedInput">
            <TextField
                label={name}
                type="number"
                InputLabelProps={{
                    shrink: true,
                }}
                fullWidth
                className="settingsTextField"
                value={value}
                onChange={handleValueChange}
            />
        </div>
    );
};

export default NumberInput;
