import React from 'react';

import './index.css';

const Divider = () => {
    
    return (
        <hr className='solid-divider'/>
    );
};

export default Divider;