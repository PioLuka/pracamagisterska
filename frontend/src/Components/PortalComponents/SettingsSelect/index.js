import React, { useEffect, useState } from "react";
import { 
    FormControl, 
    InputLabel, 
    MenuItem,
    Select
} from "@mui/material";

import './index.css';
import '../base.css';
import useComponents from "../../../ComponentsContext/useComponents";

import PropTypes from 'prop-types';

const SettingsSelect = (props) => {

    const {
        name,
        component,
        values,
        isStyle,
        isProperty
    } = props;

    const [selectedValue, setSelectedValue] = useState('');

    const { changeProps, changeStyles } = useComponents();

    useEffect(() => {
        if(isProperty){
            setSelectedValue(component.props[name]);
        } else if(isStyle) {
            setSelectedValue(component.styles[name]);
        }
    }, [component]);

    const handleChange = (event) => {
        setSelectedValue(event.target.value);
        
        if(isProperty){
            changeProps(component.id, name, event.target.value);
        } else if(isStyle) {
            changeStyles(component.id, name, event.target.value);
        }
    };

    return (
        <div className="outlinedInput">
            <FormControl fullWidth variant="outlined" className="formControl">
                <InputLabel id="selectInputLabel" className="inputLabel">{name}</InputLabel>
                <Select
                    labelId="selectInputLabel"
                    value={selectedValue || ''}
                    onChange={handleChange}
                    label={name}
                    className='outlinedInput'
                >
                    {
                        values.map((key, index) =>
                            <MenuItem key={index} value={key}>{key}</MenuItem>
                        )
                    }
                </Select>
            </FormControl>
        </div>
    );
};

SettingsSelect.propTypes = {
    name: PropTypes.string,
    component: PropTypes.object,
    values: PropTypes.array,
    style: PropTypes.bool,
    property: PropTypes.bool
};

export default SettingsSelect;
