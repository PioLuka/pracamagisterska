import React, { useEffect, useState } from "react";
import {
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from "@mui/material";

//import './index.css';
import '../base.css';
import useComponents from "../../../ComponentsContext/useComponents";
import Grid from "@mui/material/Unstable_Grid2/Grid2";

const SizeInput = (props) => {
    const {
        name,
        component
    } = props;

    const [value, setValue] = useState(0);
    const [unit, setUnit] = useState('');

    const { changeStyles } = useComponents();

    useEffect(() => {
        setValue(initValue(component.styles[name]));
        setUnit(initUnit(component.styles[name]));
    }, [component]);

    const handleValueChange = (event) => {
        if (isNaN(event.target.value) || event.target.value < 0) {
            return;
        }

        setValue(event.target.value);
        changeStyles(component.id, name, event.target.value + unit);
    }

    const handleUnitChange = (event) => {
        setUnit(event.target.value);
        if(event.target.value === 'auto') {
            setValue('');
        }
        changeStyles(component.id, name, value + event.target.value);
    };

    const initValue = (currValue) => {
        currValue = currValue.replace('px', '');
        currValue = currValue.replace('%', '');

        return currValue;
    };

    const initUnit = (currValue) => {
        if(currValue.includes('px')){
            return 'px';
        } else if(currValue.includes('%')){
            return '%';
        }else if (currValue.includes('auto')){
            return 'auto';
        }
    };

    return (
        <Grid container spacing={2}>
            <Grid md={8}>
                <div className="outlinedInput">
                    <TextField
                        label={name}
                        type="number"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        fullWidth
                        className={`settingsTextField ${unit !== 'auto' ? 'settingsTextField-enabled' : 'settingsTextField-disabled'}`}
                        value={value}
                        onChange={handleValueChange}
                        disabled={unit === 'auto'}
                    />
                </div>
            </Grid>
            <Grid md={4}>
                <div className="outlinedInput">
                    <FormControl fullWidth variant="outlined" className="formControl">
                        <InputLabel id="selectInputLabel" className="inputLabel">unit</InputLabel>
                        <Select
                            labelId="selectInputLabel"
                            label='unit'
                            className='outlinedInput'
                            value={unit || ''}
                            onChange={handleUnitChange}
                        >
                            <MenuItem value='%'>%</MenuItem>
                            <MenuItem value='px'>px</MenuItem>
                            <MenuItem value='auto'>auto</MenuItem>
                        </Select>
                    </FormControl>
                </div>
            </Grid>
        </Grid>
    );
};

export default SizeInput;
