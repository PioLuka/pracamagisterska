import React, { useEffect, useState } from "react";
import {
    FormControl,
    InputLabel,
    TextField
} from "@mui/material";

import '../base.css';
import useComponents from "../../../ComponentsContext/useComponents";

const TextInput = (props) => {
    const {
        name,
        component,
        isProperty,
        isStyle
    } = props;

    const [value, setValue] = useState('');

    const { changeProps, changeStyles } = useComponents();

    useEffect(() => {
        if(isProperty){
            setValue(component.props[name]);
        } else if(isStyle) {
            setValue(component.styles[name]);
        }
    }, [component]);

    const handleValueChange = (event) => {
        setValue(event.target.value);
        //changeProps(component.id, name, event.target.value);

        if(isProperty){
            changeProps(component.id, name, event.target.value);
        } else if(isStyle) {
            changeStyles(component.id, name, event.target.value);
        }
    }

    return (
        <div className="outlinedInput">
            <TextField
                label={name}
                InputLabelProps={{
                    shrink: true,
                }}
                fullWidth
                className="settingsTextField"
                value={value}
                onChange={handleValueChange}
            />
        </div>
    );
};

export default TextInput;
