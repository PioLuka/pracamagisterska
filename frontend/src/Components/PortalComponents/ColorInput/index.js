import React, { useEffect, useState } from "react";
import {
    Dialog,
    FormControl,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput
} from "@mui/material";
import PaletteIcon from '@mui/icons-material/Palette';

import './index.css';
import { SketchPicker } from "react-color";
import useComponents from "../../../ComponentsContext/useComponents";

const resolvePath = (object, path, defaultValue) => path
    .split('.')
    .reduce((o, p) => o ? o[p] : defaultValue, object);

const ColorInput = (props) => {

    const {
        name,
        component
    } = props;

    const [color, setColor] = useState('');
    const [openDialog, setOpenDialog] = useState(false);

    const { changeStyles } = useComponents();

    useEffect(() => {
        setColor(resolvePath(component.styles, name));
    }, [component]);

    const handleChange = () => {

    };

    const handleOpenDialog = () => {
        setOpenDialog(true);
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const handleColorChange = (newColor) => {
        setColor(newColor.hex);
    };

    const handleColorChangeComplete = (newColor) => {
        changeStyles(component.id, name, newColor.hex);
    };

    return (
        <div className="outlinedInput">
            <FormControl fullWidth variant="outlined" className="formControl">
                <InputLabel htmlFor="colorInput" className="inputLabel">{name}</InputLabel>
                <OutlinedInput
                    id="colorInput"
                    type={'text'}
                    value={color}
                    onChange={handleChange}
                    onClick={handleOpenDialog}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton
                                onClick={handleOpenDialog}
                                edge="end"
                                className="iconButton"
                            >
                                <PaletteIcon />
                            </IconButton>
                        </InputAdornment>
                    }
                    label={name}
                    className='outlinedInput'
                />
            </FormControl>

            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}

            >
                <SketchPicker
                    color={color}
                    onChange={handleColorChange}
                    onChangeComplete={handleColorChangeComplete}
                />
            </Dialog>
        </div>
    );
};

export default ColorInput;
