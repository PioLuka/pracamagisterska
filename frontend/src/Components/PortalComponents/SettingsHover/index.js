import React, { useEffect, useState } from "react";
import {
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from "@mui/material";

//import './index.css';
import '../base.css';
import useComponents from "../../../ComponentsContext/useComponents";
import Grid from "@mui/material/Unstable_Grid2/Grid2";
import ColorInput from "../ColorInput";

const SettingsHover = (props) => {
    const {
        name,
        component
    } = props;

    return (
        <div>
            {/* <div className="componentSettingsSubHeader">
                {name}
            </div> */}
            <div className='componentsSettingsInnerSubTab'>
                <ColorInput component={component} name={'hover.backgroundColor'}/>
            </div>
            {/* <div className='componentsSettingsInnerSubTab'>
                <ColorInput component={component} name={'hover.opacity'}/>
            </div> */}
        </div>
    );
};

export default SettingsHover;
