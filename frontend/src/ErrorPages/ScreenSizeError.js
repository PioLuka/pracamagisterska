import { Handyman } from "@mui/icons-material";
import React from "react";

import './index.css';

const ScreenSizeError = () => {
    return (
        <div className="errosPage">
            <Handyman
                sx={{
                    fontWeight: 600,
                    fontSize: '4rem'
                }}
            />
            <p
                className="errorTitle"
            >
                Screen size
            </p>
            <p
                className="errorMessage"
            >
                Website currently doesn't support screen size below 920px
            </p>
        </div>
    );
};

export default ScreenSizeError;