import React, { useEffect, useState } from 'react';
import './App.css';
import Portal from './Portal';
import ComponentsProvider from './ComponentsContext/ComponentsProvider.js';
import { createTheme, ThemeProvider } from '@mui/material';
import Template from './Classes/Template';
import ScreenSizeError from './ErrorPages/ScreenSizeError';

const darkTheme = createTheme({
  palette: {
    mode: 'light'
  }
});

const App = () => {

  const [windowWidth, setWindowWidth] = useState(getWindowSize());

  useEffect(() => {
    function handleWindowResize() {
      setWindowWidth(getWindowSize());
    }

    window.addEventListener('resize', handleWindowResize);

    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, []);

  function getWindowSize() {
    return window.innerWidth;
  }


  return (
    <ThemeProvider theme={darkTheme}>
      <ComponentsProvider>
        {
          windowWidth > 920 ?
          <Portal /> :
          <ScreenSizeError />
        }
      </ComponentsProvider>
    </ThemeProvider>


    // <ThemeProvider theme={darkTheme}>
    //   <div
    //     style={{
    //       height: '100vh',
    //       width: '100vw',
    //     }}
    //   >
    //     <Template />
    //   </div>
    // </ThemeProvider>
  );
}

export default App;
